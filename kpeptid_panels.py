#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
version 3.0.7
3 december 2017
Program for Peptide Analysis
"""
#
#
import wx
import os
import pickle
import string
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
from kpeptid_class import Peptide
from kpeptid_gauges import GaugeBar
from commons.info import About
from commons.warns import tell
#
TITLE = ' KimPeptid vs 3.0.7'
#
gauges = {
#                                  insol(Orange) destruc(Green)
# name   range     limits   <-->   norm     color     tooltip
'pI':  ((1, 14),  (2, 10),    1, (  3,   9),  False, "Isoelectric Point"),
'AH':((-20, 20), (-6, 0.1),   0, (  0,   5),   True, "Average Hydrophobicity"),
'RH': ( (0,  4),  (0.8, 1.9), 1, (  0.8, 2.6), True, "Hydrophobic Ratio"),
'RHP': ((0,  5),  (0.7, 1.4), 1, (  0.7, 1.1), True, "%Hydrophobic Ratio"),
'LIND':((0,  1),  (0.2, 0.4), 1, (  0.2, 0.3), True, "Linear Charge Density"),
'sol': ((0, 14),  (1.1, 2.5), 1, (  1.1, 2.1), True, "Solubility"),
'Rt':((-20, 80), (-5,  10),   1, (-10,  10),  False, "Retention Time")}
#
# noinspection PyPep8Naming
def get_scale_data():
    """"""
    HYDROSCALE = 'Hydrophobicity (partition coefficients) Scales '
    WINDOW = 'Window width for hydropathy plots'
    RETENTION = 'HPLC retention coefficient Scales'

    vars_ = {'hydrophobicity':
             [HYDROSCALE, 'hdrscl', 'K&D',
              'K&D', 'F&P', 'ROS', 'C&W7', 'C&W3', 'ESKV', 'GES'],
             'retention':
             [RETENTION, 'retent', 'MJL_2.1',
              'MJL_2.1', 'MJL_7.4', 'BBS_TFA'],
             'window':
             [WINDOW, 'window', '3',
             '3', '5', '10', '20']}
    return vars_
#
#
class ToolBar(NavigationToolbar2Wx):
    """A Customized Toolbar"""
    def __init__(self, parent, xtext, ytext):
        NavigationToolbar2Wx.__init__(self, parent)
        self.SetToolBitmapSize(wx.Size(10, 15))
        self.DeleteToolByPos(7)   # subplots
        self.DeleteToolByPos(6)   # separator
        self.DeleteToolByPos(2)   # right
        self.DeleteToolByPos(1)   # left
        xpos = 150
        xlabel = wx.StaticText(self, -1, label=xtext,
                               pos=(xpos, 6), size=(23, 15),
                               style=wx.ALIGN_RIGHT)
        ylabel = wx.StaticText(self, -1, label=ytext,
                               pos=(xpos + 68, 6), size=(25, 15),
                               style=wx.ALIGN_RIGHT)
        #
        self.tc_x = wx.TextCtrl(self, -1,
                                pos=(xpos + 25, 3), size=(40, 22))
        self.tc_y = wx.TextCtrl(self, -1,
                                pos=(xpos + 96, 3), size=(40, 22))
        #
        toolbar_color = self.GetBackgroundColour()
        xlabel.SetBackgroundColour(toolbar_color)
        ylabel.SetBackgroundColour(toolbar_color)
        #
        font = wx.Font(8, wx.FONTFAMILY_MODERN,
                       wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        #
        self.tc_x.SetFont(font)
        self.tc_y.SetFont(font)
        #
        self.Realize()
        self.update()

# noinspection PyArgumentList
class CanvasPanel(wx.Panel):
    def __init__(self, parent, pos=wx.DefaultPosition,
                 size=(150, 200), xlabel='x', ylabel='y', tx='', ty=''):
        wx.Panel.__init__(self, parent, pos=pos, size=size,
                          style=wx.RAISED_BORDER)
        #
        self.SetBackgroundColour("white")
        # noinspection PyPep8Naming
        self.SetMinSize = (170, 90)
        self.figure = Figure(figsize=(1, 1))   # (x,y) en inches!!
        self.axes = self.figure.add_subplot(111)
        self.xlabel = xlabel
        self.ylabel = ylabel
        #
        self.canvas = FigureCanvas(self, -1, self.figure)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.toolbar = ToolBar(self.canvas, tx, ty)
        self.tc_x = self.toolbar.tc_x
        self.tc_y = self.toolbar.tc_y
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer.Add(self.toolbar, 0, wx.LEFT | wx.EXPAND)
        self.SetSizer(sizer)
        # self.Fit()
        #
        self.xlim = (1, 5)
        self.ylim = (1, 5)
        self.draw()
#
#
    def draw(self, refresh=False):
        """ """
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabel)
        if refresh:
            self.axes.set_xlim(self.xlim)
            self.axes.set_ylim(self.ylim)
            #
        self.canvas.draw()
#
#
class GaugesPanel(wx.Panel):
    def __init__(self, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)
        sizer = wx.FlexGridSizer(7, 4, 5, 5)

        self.gobjects = {}
        flag = wx.SizerFlags().Expand().Border(wx.ALL, 1)
        flag1 = wx.ALIGN_LEFT

        # glist    -> name  range  limits  <-->  norm   color   tooltip
        # GaugeBar ->       rang,  limits, norm,  rev,     color, pos, size
        for gname, glist in gauges.items():
            gobject = GaugeBar(self,
                               glist[0], glist[1], glist[3], glist[2], glist[4])
            gobject.SetToolTip(glist[-1])
            self.gobjects[gname] = gobject

            # column #0
            sizer.Add(wx.StaticText(self, label='  ' + gname,
                                    size=(50, 20), style=flag1), flag)
            # column #1
            sizer.Add(wx.StaticText(self, label=str(glist[0][0]),
                                    size=(20, 15), style=wx.ALIGN_RIGHT), flag)
            # column #2
            sizer.Add(gobject, flag)
            # column #3
            sizer.Add(wx.StaticText(self, label=str(glist[0][1]),
                                    size=(15, 15), style=flag1), flag)

        self.SetSizer(sizer)
        sizer.AddGrowableCol(2)
#
#
class ParamSelectPanel(wx.Panel):
    def __init__(self, parent, pos=wx.DefaultPosition, size=(150, 65)):
        wx.Panel.__init__(self, parent, pos=pos, size=size)
        size_label = (100, 20)
        size_box = (80, 20)
        self.SetBackgroundColour("yellow")
        self.SetMinSize(size)
        self.sizer = wx.GridSizer(rows=3, cols=2, hgap=0, vgap=0)
        self.properties = {}
        self.build_selector_choices(size_box, size_label)
        self.SetSizer(self.sizer)
        self.Fit()

    # noinspection PyArgumentList
    def build_selector_choices(self, size_box, size_label):
        flag1 = wx.ALIGN_LEFT
        scales = get_scale_data()

        for eachLabel, eachList in scales.items():
            self.sizer.Add(wx.StaticText(self, label='  ' + eachLabel,
                                         size=size_label, style=flag1))
            widget = wx.ComboBox(self, -1, eachList[2], choices=eachList[3:],
                                 size=size_box, style=wx.CB_DROPDOWN)
            widget.SetToolTip(eachList[0])
            self.sizer.Add(widget, 1, flag1)
            self.properties[eachLabel] = widget
#
#
# noinspection PyUnusedLocal
class PeptideSelectPanel(wx.Panel):
    """peptides -> dictionary peptides={key1: peptide1, ....}
            -key is a unique reference like ASDF_n
            -peptide1 is an instance of the Peptide class
    Peptide instances have several pre-calculated attributes
    and several methods.
    Thus their storage and reloading with pickle requires the original class.
    Could save only the sequence...

    """
    peptides = dict()
    # noinspection PyArgumentList
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, -1, size=(100, 100))
        sizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer2 = wx.BoxSizer(wx.VERTICAL)
        self.chklst = wx.CheckListBox(self, size=(150, 50), pos=(20, 10))
        self.buttons = wx.Panel(self, size=(50, 100))
        self.peptide_defaults = PepDefault(self.buttons)

        buttons = dict(Load=('LOAD', 'on_load'),
                       Merge=('MERGE', 'on_merge'),
                       Save=('SAVE', 'on_save'),
                       Refresh=('REFRESH', 'on_refresh'),
                       Del=('DELETE', 'on_delete'),
                       DelAll=('DEL ALL', 'on_del_all'),
                       Report=('REPORT', 'on_report')
                       )
        self.properties = {}
        for name, prop in buttons.items():
            widget = wx.Button(self.buttons, -1, prop[0], size=(-1, 22))
            self.Bind(wx.EVT_BUTTON, getattr(self, prop[1]), widget)
            sizer2.Add(widget)
            self.properties[name] = widget
            #
        sizer2.Add(wx.Panel(self.buttons, size=(-1, 12)))
        self.bt_show = wx.Button(self.buttons, -1, 'SHOW', size=(-1, 22))
        self.Bind(wx.EVT_BUTTON, self.on_show, self.bt_show)
        sizer2.Add(self.bt_show)
        sizer2.Add(wx.Panel(self.buttons, size=(20, 10)), 0, wx.EXPAND)
        sizer2.Add(self.peptide_defaults)
        #
        sizer2.Add(wx.Panel(self.buttons, size=(-1, 12)))
        self.bt_readme = wx.Button(self.buttons, -1, 'README', size=(-1, 22))
        self.Bind(wx.EVT_BUTTON, self.on_readme, self.bt_readme)
        sizer2.Add(self.bt_readme)
        sizer2.Add(wx.Panel(self.buttons, size=(20, 10)), 0, wx.EXPAND)
        #
        self.buttons.SetSizer(sizer2)
        self.buttons.Fit()
        #
        sizer.Add(wx.Panel(self, size=(20, 10)), 0, wx.EXPAND)
        sizer.Add(self.chklst, 0, wx.EXPAND)
        sizer.Add(wx.Panel(self, size=(20, 10)), 0, wx.EXPAND)
        sizer.Add(self.buttons, 1, wx.EXPAND)
        #
        self.SetSizer(sizer)
        self.Fit()

    def on_load(self, event):
        """Loads pickled Peptide objects
        """
        wildcard = "Peptide File (*.pep)|*.pep|"\
                   "Text (*.txt)|*.txt|"\
                   "All files (*.*)|*.*"

        fileopen = get_file(wildcard, 'open')
        #
        if not os.path.exists(fileopen):
            return

        if fileopen.rsplit('.')[-1] != 'pep':
            self.peptides = convert_text_to_peptides(fileopen)
        else:
            try:
                self.peptides = pickle.load(open(fileopen, 'rb'))
            # AttributeError -> p.e. wrong version of pickled file
            except (IOError, AttributeError):
                tell('The file <%s> could not be read' % fileopen)
                return
            #
        self.chklst.Clear()
        for key in self.peptides:
            self.chklst.Append(key)
            #
        peptide = list(self.peptides.values())[0]
        scales = get_scale_data()
        #
        for key in scales:
            # noinspection PyTypeChecker
            index = getattr(peptide, key) + 3
            name = scales[key][index]
            self.peptide_defaults.properties[key].SetValue(name)

    def on_save(self, event):
        """Serializes list of Peptide objects"""
        wildcard = "Peptide File (*.pep)|*.pep|"\
                   "All files (*.*)|*.*"

        # noinspection PyArgumentEqualDefault
        file_save = get_file(wildcard, 'save')
        #
        if file_save:
            try:
                pickle.dump(self.peptides, open(file_save, 'wb'))
            except IOError:
                tell('Document <%s> could not be saved' % file_save)

    def on_report(self, event):
        """Save peptides data in csv (comma separated values) format
        """
        wildcard = "Comma separated values (*.csv)|*.csv|"\
                   "All files (*.*)|*.*"

        # noinspection PyArgumentEqualDefault
        file_report = get_file(wildcard, 'save')
        #
        if not file_report:
            return

        have_header = False
        pept_list_data = []

        for key in self.peptides:
            peptide_data = []
            peptide_attributes = vars(self.peptides[key])
            if not have_header:
                attributes = peptide_attributes.keys()
                header = ','.join(attributes)
                pept_list_data.append(header)
                have_header = True
            # noinspection PyUnboundLocalVariable
            for attribute in attributes:
                peptide_data.append(str(peptide_attributes[attribute]))

            line = ','.join(peptide_data)
            pept_list_data.append(line)

        full_text = '\n'.join(pept_list_data)

        try:
            # noinspection PyTypeChecker
            open(file_report, 'w').write(full_text)
        except IOError:
            tell('Document <%s> could not be saved' % file_report)

    def on_merge(self, event):
        """"""
        wildcard = "Peptide File (*.pep)|*.pep|"\
                   "All files (*.*)|*.*"
        dialog = wx.FileDialog(None, "Choose a file", os.getcwd(),
                               "", wildcard, wx.FD_OPEN)

        file_to_merge = ''
        if dialog.ShowModal() == wx.ID_OK:
            file_to_merge = dialog.GetPath()
        dialog.Destroy()
        #
        if file_to_merge:
            try:
                new_list = pickle.load(open(file_to_merge))
            except (pickle.UnpicklingError, EOFError):
                tell('The file could not be loaded or is empty')
                return
        else:
            return
        #
        new_peptide = new_list.values()[0]
        base_peptide = self.peptides.values()[0]
        #
        try:
            win = (new_peptide.window == base_peptide.window)
            hid = (new_peptide.hydrophobicity == base_peptide.hydrophobicity)
            ret = (new_peptide.retention == base_peptide.retention)
        except AttributeError:
            tell('The parameter doesnt exist')
            return
        #
        if (win or hid or ret) is False:
            tell('Files are not compatible')
            return
        #
        for key, contents in new_list.iteritems():
            pass_this = False
            for second_key in self.peptides:
                if contents.sequence == self.peptides[second_key].sequence:
                    pass_this = True
                    break
            if pass_this:
                continue
            key_old = key
            i = 0
            while True:
                if key in self.peptides:
                    i += 1
                    key = '%s_%i' % (key_old, i)
                else:
                    break
            self.peptides[key] = contents
        #
        self.chklst.Clear()
        for key in self.peptides:
            self.chklst.Append(key)
    #
    # noinspection PyMethodMayBeStatic
    def on_refresh(self, event):
        event.Skip()
    #
    def on_delete(self, event):
        count = self.chklst.GetCount()
        for index in range(count):
            item = count - index - 1
            if self.chklst.IsChecked(item):
                key = self.chklst.GetString(item)
                self.chklst.Delete(item)
                self.peptides.pop(key)
    #
    def on_del_all(self, event):
        self.chklst.Clear()
        self.peptides.clear()
        for key in get_scale_data():
            self.peptide_defaults.properties[key].Clear()
    #
    # noinspection PyMethodMayBeStatic
    def on_show(self, event):
        event.Skip()
    #
    # noinspection PyMethodMayBeStatic
    def on_readme(self, event):
        About().Show()
#
#
class PepDefault(wx.Panel):
    """"""
    def __init__(self, parent, pos=wx.DefaultPosition, size=(95, 65)):
        wx.Panel.__init__(self, parent, pos=pos, size=size)
        size_label = (45, 17)
        size_txt_ctrl = (35, 17)
        self.SetBackgroundColour("yellow")
        self.SetMinSize(size)
        self.sizer = wx.GridSizer(rows=3, cols=2, hgap=1, vgap=1)
        self.properties = {}
        self.build_stats_window(size_txt_ctrl, size_label)
        self.SetSizer(self.sizer)
        self.Fit()
    #
    # noinspection PyArgumentList
    def build_stats_window(self, sz_tc, sz_st):
        flag = wx.ALIGN_LEFT
        scales = get_scale_data()
        for label, values in scales.items():
            st = wx.StaticText(self, label=values[1], size=sz_st, style=flag)
            widget = wx.TextCtrl(self, -1, size=sz_tc, style=wx.TE_READONLY)
            widget.SetToolTip(values[0])
            self.sizer.Add(st)
            self.sizer.Add(widget, 1, flag)
            self.properties[label] = widget
#
#
def convert_text_to_peptides(fileopen, n=0):
    """Builds a dictionary of Peptide objects from sequences in a text file.

     The text file has One Sequence per line.
     Admits files with several tab separated columns with sequence in column n.

    """
    peptides = PeptideSelectPanel.peptides
    peptides.clear()
    letters_allowed = string.digits + string.ascii_letters + '_'
    letters_forbidden = string.digits + string.whitespace
    for line in open(fileopen):
        # if line.startswith('>'):continue         #can read data bases
        sequence = line.split('\t')[n]
        sequence = sequence.strip(letters_forbidden)
        key_old = key = sequence[:7]
        i = 0
        while True:
            if key in peptides:
                i += 1
                key = '%s_%i' % (key_old, i)
            else:
                break

        key = ''.join(letter for letter in key if letter in letters_allowed)
        peptides[key] = Peptide(sequence)

    return peptides
#
#
def get_file(wildcard, mode='save'):
    """Dialog to ask for a filename.

    mode = 'save' / 'open'

    """
    file_path = ''
    mode = wx.FD_SAVE if mode == 'save' else wx.FD_OPEN
    dialog = wx.FileDialog(None, "Choose a file", os.getcwd(),
                           "", wildcard, mode)
    if dialog.ShowModal() == wx.ID_OK:
        file_path = dialog.GetPath()
    dialog.Destroy()

    return file_path


if __name__ == '__main__':

    class AFrame(wx.Frame):
        def __init__(self, *args, **kwargs):
            # noinspection PyPep8Naming
            MyPanel = kwargs.pop('panel')
            wx.Frame.__init__(self, *args, **kwargs)
            self.panel = MyPanel(self)

    app = wx.App()

    ALL_PANELS = [CanvasPanel, PeptideSelectPanel,
                  PepDefault, ParamSelectPanel, GaugesPanel]

    for Panel in ALL_PANELS:
        AFrame(None, panel=Panel).Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
