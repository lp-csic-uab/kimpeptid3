
---

**WARNING!**: This is the *Old* source-code repository for KimPeptid3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimpeptid3/) located at https://sourceforge.net/p/lp-csic-uab/kimpeptid3/**  

---  
  
![https://lh3.googleusercontent.com/zaM9UzIbvzwwHXofikZsIq6aHRAxFnB0tBW1yke5cp4gOLwiD56P_zLA0YjOGVtO8F82PCxXZSUK7ibCu6bgc_xNDjhcn4SVjQ_y0mrigZSU5cLpPgX8_7oDZws_RVFSttjasfFh-1Dgtp3fN1GsxH96od98yaXzXyfu32IrKhWeMCB9rlTW3PhmKm24zdtBDIff2yhH7MVoeaD5qI5tRtT-csZP4saZ89uCKK0wlPZGml5SM08s0Syz2nRvF_NooZtL-4lFxeh5cJiy6ctZ7UNBo3Eh7djJzGGePnYps2DwRA6s-e2BU6uB6y5bNaG0S4r0Z1OIUn7ELZkEYdD9EvE0rNziUD10oAoBcLrQZfR8QgJ_cJTbpy9qkoCJvbdauWwxEwJt90rStJIl_ksl5sV0TPs2WVVkQ1YcxQZpmOE4bes9wy56dDiCqd-aPMuQe_oei2szoMxxUJrFnBLHf8eLRb29wi1OPESsLufAkMK50O1oo_K6ZQYbgLGSR0yTpirZI2w1MGQf-rBYhStMrOdj6B8V5VdDs-6glIiGJE2Ly-PHvIuiYz3YCnmoDP5KCYU9bdaCcaHtT_A9CCkMlneex1pAe0BovtQRWVbl4FPxw3_XjERA=s53-no](https://lh3.googleusercontent.com/zaM9UzIbvzwwHXofikZsIq6aHRAxFnB0tBW1yke5cp4gOLwiD56P_zLA0YjOGVtO8F82PCxXZSUK7ibCu6bgc_xNDjhcn4SVjQ_y0mrigZSU5cLpPgX8_7oDZws_RVFSttjasfFh-1Dgtp3fN1GsxH96od98yaXzXyfu32IrKhWeMCB9rlTW3PhmKm24zdtBDIff2yhH7MVoeaD5qI5tRtT-csZP4saZ89uCKK0wlPZGml5SM08s0Syz2nRvF_NooZtL-4lFxeh5cJiy6ctZ7UNBo3Eh7djJzGGePnYps2DwRA6s-e2BU6uB6y5bNaG0S4r0Z1OIUn7ELZkEYdD9EvE0rNziUD10oAoBcLrQZfR8QgJ_cJTbpy9qkoCJvbdauWwxEwJt90rStJIl_ksl5sV0TPs2WVVkQ1YcxQZpmOE4bes9wy56dDiCqd-aPMuQe_oei2szoMxxUJrFnBLHf8eLRb29wi1OPESsLufAkMK50O1oo_K6ZQYbgLGSR0yTpirZI2w1MGQf-rBYhStMrOdj6B8V5VdDs-6glIiGJE2Ly-PHvIuiYz3YCnmoDP5KCYU9bdaCcaHtT_A9CCkMlneex1pAe0BovtQRWVbl4FPxw3_XjERA=s53-no)


---

**WARNING!**: This is the *Old* source-code repository for KimPeptid3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimpeptid3/) located at https://sourceforge.net/p/lp-csic-uab/kimpeptid3/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

**KimPeptid** is a rich GUI based, [Python](http://en.wikipedia.org/wiki/Python_%28programming_language%29) application that calculates several important _peptide properties_ (such as [pI](http://en.wikipedia.org/wiki/Isoelectric_point), [hydropathicity](http://dx.doi.org/10.1016/0022-2836(82)90515-0), charge, [monoisotopic mass](http://en.wikipedia.org/wiki/Monoisotopic_mass), etc ...) and helps to easily analyze the results.

![https://lh5.googleusercontent.com/-sG3djlne4zs/T2DU6NQ8s2I/AAAAAAAAATY/7RMbGwvHpqA/s640/Kimpeptid2.png](https://lh5.googleusercontent.com/-sG3djlne4zs/T2DU6NQ8s2I/AAAAAAAAATY/7RMbGwvHpqA/s640/Kimpeptid2.png)

#### Features

  * Hydropathy plot using different hydropathy scales from literature.
  * Charge/pH plot
  * Group of gauges with marks of normal ranges and indication with colored bars of the protein solubility characteristics related with some of them.
Gauges include:
    * retention time
    * linear charge density (LIND)
    * average hydrophobicity (AH)
    * Ratio of hydrophobic percentages (RHP)
    * Isoelectric Point
    * hydrophobic ratio (RH)
    * solubility
    * Simulated 2D LC separation of peptides using theoretical retention times and charge at pH 3
    * Serialization (pickling) of Peptide object collections
    * Save peptide list and corresponding parameters as csv file

SCALES

Dissociation Constants

TODO (pKa, pKb and pKr)

Hydrophobicity

The free energy differences obtained from transferring amino acid chains from an organic solvent to water, are called the partition coefficients, logP, or hydrophobicity scales.
Available scales are:

| K&D | Hydrophobicity Scale, | Kyte&Doolittle J.Mol.Bio. 157:105-132 (1982) |
|:----|:----------------------|:---------------------------------------------|
| F&P | Hydrophobicity Scale (pi-r) | Fauchere&Pliska Eur.J.Med.Chem.-Chim.Ther.18:369-375 (1983) |
| ROS | Hydrophobicity scale (pi-r) | Roseman M.A. J. Mol. Biol. 200:513-522 (1988). |
| C&W7 | Hydrophobicity indices at pH 7.5 determined by HPLC. | Cowan R., Whittaker R.G. Peptide Research 3:75-80 (1990) |
| C&W3 | Hydrophobicity indices at pH 3.4 determined by HPLC. | Cowan R., Whittaker R.G. Peptide Research 3:75-80 (1990) |
| ESKW | Normalized consensus hydrophobicity. Average of 5 other scales | Eisenberg, Schwarz, Komaromy & Wall J. Mol. Biol. 179:125-142 (1984) |
| GES |                       | Goldman,Engelman & Steitz Annu.Rev.Biophys.Biophys.Chem. 15:321-353 (1986) |

Hydropathy plots

Hydropathy plots allow visualization of hydrophobicity over the length of a peptide sequence.

Plots are useful in determining the hydrophobic interior portions of globular proteins as well as determining membrane spanning regions of membrane bound proteins.

HPLC Retention Times

This are peptide coefficients calculated from different retention coefficient tables found in the literature.
Available scales are:

| MJL2 | Retention coefficient in HPLC, pH 2.1 | Meek J.L., Proc. Natl. Acad. Sci. USA 77:1632-1636(1980) |
|:-----|:--------------------------------------|:---------------------------------------------------------|
| MLJ7 | Retention coefficient in HPLC, pH 7.4 | Meek J.L. Proc. Natl. Acad. Sci. USA 77:1632-1636(1980)  |
| BBS  | Retention coefficient in TFA          | Browne C.A., Bennett H.P.J., Solomon S. Anal. Biochem. 124:201-208(1982) |

#### Installation

Tested in Windows XP 32 bits and Windows 7 64 bits.

##### Requirements

  * No special requirements.

##### From Installer

  1. Download `KimPeptid_x.y_setup.exe` windows installer from repository at the [KimPeptid project download page](https://bitbucket.org/lp-csic-uab/kimpeptid/downloads).
  1. [Get your Password](#Download.md) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `KimPeptid` by double-clicking on the `KimPeptid` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install Python and third party software indicated in [Dependencies](#Source_Dependencies:.md).
  1. Download `KimPeptid` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/kimpeptid/src).
  1. Download commons source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons/src).
  1. Copy the folders in python site-packages or in another folder in the python path.
  1. Run `KimPeptid` by double-clicking on the `kpeptid_main.pyw` module.

###### _Source Dependencies:_

  * [Python](http://www.python.org/) 2.6 or 2.7 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 2.8.11.0 - 2.8.11.2
  * [matplotlib 1.1.0](http://matplotlib.sourceforge.net/)
  * [numpy 1.6.1](http://www.scipy.org/)

Third-party program versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

##### Post-Install

To Do!

#### Download

You can download the last version of **KimPeptid** [here](https://bitbucket.org/lp-csic-uab/kimpeptid/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/kimpeptid/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program.

#### Change-Log

0.67 March 2012

  * English translations
  * Extensive refactoring
  * relocation of code in modules
  * Fix bugs in gauges
  * version number on module names eliminated

0.65 september 2010

  * Change application name from Peptido to KimPeptid

#### To-Do

  * Finish this readme document

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)


---

**WARNING!**: This is the *Old* source-code repository for KimPeptid3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimpeptid3/) located at https://sourceforge.net/p/lp-csic-uab/kimpeptid3/**  

---  
  
