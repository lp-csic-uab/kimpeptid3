#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
version 3.0.7
3 december 2017
Program for Peptide Analysis
"""
#
#
import wx
from wx.lib.agw.pygauge import PyGauge
#
#
class GaugeBar(PyGauge):
    def __init__(self, parent, rang=(1, 14), limits=(3, 10),
                 norm=(4, 9), rev=0, color=True, pos=(0, 0), size=(50, -1)):
        self.range_min, self.range_max = rang
        self.range_width = rang[1] - rang[0]
        self.limit_min, self.limit_max = limits
        self.normal_min, self.normal_max = norm
        self.rev = rev
        self.color = color
        self.factor = 1
        #
        if self.range_width <= 10:
            self.factor = 10
        if self.range_width <= 1:
            self.factor = 100
        #
        range_factor = self.range_width * self.factor
        #
        PyGauge.__init__(self, parent, -1, range_factor,
                         pos, size,
                         style=wx.GA_HORIZONTAL | wx.FULL_REPAINT_ON_RESIZE)
        #
        # Copy the default font, make it bold.
        self.font = wx.Font(self.GetFont().GetPointSize(),
                            self.GetFont().GetFamily(),
                            self.GetFont().GetStyle(),
                            wx.FONTWEIGHT_BOLD
                            )
        #
        self.raw_value = 0
        self.SetValue(0)
        self.SetBackgroundColour('light blue')
        self.SetForegroundColour('orange')
        #
        self.Bind(wx.EVT_PAINT, self.onPaint)
        self.Fit()
    #
    def update_gauges(self, value):
        """Updates the gauge, sets the raw value for the next EVT_PAINT
        """
        color = 'blue'
        #
        if self.color:
            if value > self.limit_max:     # >10
                color = 'green' if self.rev else 'orange'
            elif value < self.limit_min:   # <2
                color = 'orange' if self.rev else 'green'
            else:
                color = 'yellow'
        #
        self.SetBarColour(color)
        final_value = (value - self.range_min) * self.factor
        self.raw_value = value

        # noinspection PyBroadException
        try:
            self.SetValue(round(final_value))
        except Exception:
            if final_value < self.range_min:
                self.SetValue(0)
            elif final_value > self.range_max:
                self.SetValue(round(self.range_width * self.factor))
    #
    # noinspection PyArgumentList,PyPep8Naming
    def onPaint(self, evt):

        self.Refresh()
        dc = wx.PaintDC(self)
        dc.Clear()
        #
        PyGauge.OnPaint(self, evt)
        #
        w, h = dc.GetSize()

        if self.raw_value > 200:
            txt = '> 200'
        else:
            txt = '%.2f' % self.raw_value
        #
        # Set the font for the DC ...
        dc.SetFont(self.font)
        #
        fw, fh = dc.GetTextExtent(txt)
        #
        tx = (w / 2) - (fw / 2)
        ty = (h / 2) - (fh / 2)
        #
        dc.SetTextForeground(wx.BLACK)
        dc.DrawText(txt, tx, ty)
        dc.SetTextForeground(wx.WHITE)
        dc.DrawText(txt, tx - 1, ty - 1)
        #
        relative_normal_min = self.normal_min - self.range_min
        relative_normal_max = self.normal_max - self.range_min
        xmin = relative_normal_min * w / self.range_width
        xmax = relative_normal_max * w / self.range_width
        #
        dc.SetTextForeground(wx.BLACK)
        dc.DrawText('|', xmin, -8)
        dc.DrawText('|', xmin, 10)
        dc.DrawText('|', xmax, -8)
        dc.DrawText('|', xmax, 10)
#
#
if __name__ == '__main__':

    class AFrame(wx.Frame):
        def __init__(self, *args, **kwargs):
            wx.Frame.__init__(self, *args, **kwargs)

            self.gauge = GaugeBar(self)

            p4 = wx.StaticText(self, wx.ID_ANY, "1")
            p5 = wx.StaticText(self, wx.ID_ANY, "1")

            sizer = wx.FlexGridSizer(1, 3, 1, 1)
            sizer.Add(p4, 1, wx.EXPAND)
            sizer.Add(self.gauge, 1, wx.EXPAND)
            sizer.Add(p5, 1, wx.EXPAND)

            self.SetSizer(sizer)
            sizer.AddGrowableCol(1)
            self.Layout()
            self.SetSize((400, 100))


    app = wx.App()
    a_frame = AFrame(None)
    a_frame.gauge.update_gauges(13)
    a_frame.Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
