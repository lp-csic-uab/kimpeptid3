; For All
; J.Abian 30 july 2010 - O.Gallardo july 2014

#define MyAppName ReadIni(SourcePath + "install.ini", "Common", "name", "noname")
#define MyComment ReadIni(SourcePath + "install.ini", "Common", "comment", "nocomment")
#define MyVersion ReadIni(SourcePath + "install.ini", "Common", "version", "0.0.0")
#define MyAppCopyright ReadIni(SourcePath + "install.ini", "Common", "copyright", "Pending LP-CSIC/UAB")
#define MyAppWeb ReadIni(SourcePath + "install.ini", "Common", "web", "https://code.google.com/p/lp-csic-uab/")
#define MySourceDir ReadIni(SourcePath + "install.ini", "Common", "srcmaindir", "dist")
#define MySourceDocDir ReadIni(SourcePath + "install.ini", "Common", "srcdocdir", "doc")
#define MySourceImageDir ReadIni(SourcePath + "install.ini", "Common", "srcimgdir", "images")
#define MyWizardDir ReadIni(SourcePath + "install.ini", "Common", "srcimgdir", "images")
#define MyBigIcon ReadIni(SourcePath + "install.ini", "Common", "big_icon", "")
#define MySmallIcon ReadIni(SourcePath + "install.ini", "Common", "small_icon", "")
#define MyWizardImage ReadIni(SourcePath + "install.ini", "Inno", "wzimg", "")
#define MyWizardSecondImage ReadIni(SourcePath + "install.ini", "Inno", "wzsimg", "")
#define MyPswd ReadIni(SourcePath + "install.ini", "Inno", "pswd", "lpcsicuab")
#define MyInstall ReadIni(SourcePath + "install.ini", "Inno", "install", "INSTALL.txt")
#define MyReadMe ReadIni(SourcePath + "install.ini", "Inno", "readme", "README.txt")
#define MyLicence ReadIni(SourcePath + "install.ini", "Inno", "licence", "LICENCE.txt")

[Setup]
AppName={#MyAppName}
;data for unins000.dat file
AppId={#MyAppName} {#MyVersion}
;appears in the first page of the installer
;AppVerName={cm:NameAndVersion,{#MyAppName},{cm:Myvers}}
;appears in the support info for add/remove programs
AppVersion={#MyVersion}
AppPublisher=LP-CSIC/UAB
DefaultDirName={pf}\{#MyAppName}_{#MyVersion}
UsePreviousAppDir=no
DefaultGroupName=LP-CSIC-UAB
Compression=lzma/ultra64
AllowNoIcons=yes
AllowRootDirectory=no
UsePreviousLanguage=no
UninstallDisplayIcon={#MySourceImageDir}\{#MySmallIcon}
OutputBaseFilename={#MyAppName}_{#MyVersion}_setup
OutputDir=installer
InfoAfterFile={#MyInstall}
LicenseFile={#MyLicence}
Password={#MyPswd}
WizardImageFile={#MyWizardDir}\{#MyWizardImage}
#ifdef MyWizardSecondImage
  WizardSmallImageFile={#MyWizardDir}\{#MyWizardSecondImage}
#endif
AppCopyright={#MyAppCopyright}
;appears in properties "version del archivo" and "version del producto"
;of the Setup.exe program in the "Version" page
;and also in the info when Setup.exe is selected with the cursor and where it adds a zero
VersionInfoVersion={#MyVersion}
SetupIconFile={#MySourceImageDir}\{#MyBigIcon}

[Files]
;Main App files:
Source: "{#MySourceDir}\*"; DestDir: "{app}"
Source: {#MySourceImageDir}\{#MySmallIcon}; DestDir: {app}
Source: {#MySourceImageDir}\{#MyBigIcon}; DestDir: {app}
#if FileExists(SourcePath+"\"+MySourceDir+"\mpl-data\matplotlibrc")
  Source: "{#MySourceDir}\mpl-data\*"; DestDir: "{app}\mpl-data"
  Source: "{#MySourceDir}\mpl-data\images\*"; DestDir: "{app}\mpl-data\images"
#endif

;Documentation and Other documents:
#if FileExists(SourcePath+"\"+MySourceDocDir+"\README.html")
  Source: "{#MySourceDocDir}\*"; DestDir: "{app}\doc"
#endif
Source: {#MyReadMe}; DestDir:{app}
Source: {#MyLicence}; DestDir:{app}
Source: {#MyInstall}; DestDir:{app}

;Test files:
;Source: "test\*"; DestDir: "{app}\test"

;Source Code files:
;Source: "*"; DestDir: "{app}\src"

[Tasks]
;CreateDesktopIcon is defined in Default.isl
Name: desktopicon; Description: "{cm:CreateDesktopIcon}"

[Icons]
;Applications Menu Icons:
Name: "{group}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe" ; IconFilename:{app}\{#MySmallIcon}; WorkingDir: "{app}"; Comment: {#MyComment}
#if FileExists(SourcePath+"\"+MySourceDocDir+"\README.html")
  Name: "{group}\{#MyAppName} Documentation"; Filename: "{app}\doc\README.html" ;WorkingDir: "{app}\doc"; Comment: "{#MyAppName} Documentation"
#endif
Name: "{group}\{#MyAppName} Web Page"; Filename: {#MyAppWeb}; Comment: {#MyAppWeb}
Name: "{group}\LP-CSIC-UAB Web Page"; Filename: "http://proteomica.uab.cat"; Comment: "http://proteomica.uab.cat"
Name: "{group}\UnInstall {#MyAppName} {#MyVersion}"; Filename: "{uninstallexe}"; Comment: "Remove {#MyAppName} {#MyVersion} from this system"

;Desktop Icons:
Name: "{userdesktop}\{#MyAppName} {#MyVersion}"; Filename: "{app}\{#MyAppName}.exe"; IconFilename:{app}\{#MyBigIcon}; WorkingDir: "{app}"; Comment: {#MyComment}; Tasks: desktopicon
