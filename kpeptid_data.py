#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
version 0.68
13 october 2010
Program for Peptide Analysis
"""
import os

TITLE = ' KimPeptid vs.0.68'
ICON = os.path.join("images", "kpeptid32.ico")
# ICON = os.path.join("C:/Python36/programas/kimpeptid/images", "kpeptid32.ico")      #For PC
# ICON= os.path.join("img", "pep.ico")               #For py2exe
#
# The free energy differences obtained from transferring amino acid chains from
# an organic solvent to water, are called the partition coefficients, logP,
# or hydrophobicity scales.
#
# Kyte&Doolittle Hydrophobicity Scale            (KD)
#       J.Mol.Bio.(1982)157 105-132
# Fauchere&Pliska Hydrophobicity Scale (pi-r)    (FP)
#       Eur.J.Med.Chem.-Chim.Ther.18(1983)369-375
# Roseman M.A. Hydrophobicity scale (pi-r)       (RO).
#       J. Mol. Biol. 200:513-522(1988).
# Cowan R., Whittaker R.G.
# Hydrophobicity indices at ph 7.5 determined by HPLC. (CW7)
# Cowan R., Whittaker R.G.
# Hydrophobicity indices at ph 3.4 determined by HPLC. (CW3)
#       Peptide Research 3:75-80(1990).
# Eisenberg, Schwarz, Komaromy & Wall            (ES)
#       J. Mol. Biol. 179 (1984), 125-142
#      (normalized consensus hydrophobicity values. Average of 5 other scales.)
# Goldman,Engelman & Steitz                      (GES)
#      Annu.Rev.Biophys.Biophys.Chem. 15 (1986),321-353
#      (from D.R.Nelson and H.W.Strobel, JBC 263 (1988) 6038-6050
#       where there is also a KD table. Seems the sign of KD values in this
#       paper is the opposite to that in the dictionary of the program.
#       According to this I changed also the sign of GES.
#       It should be checked if sign had been changed at some moment (maybe for
#       homogeneity?) and document it.
#       In any case, KD values in table agree with those in expasy :
#       (http://us.expasy.org/tools/pscale/Hphob.Doolittle.html))
#
KD = 0
FP = 1
RO = 2
CW7 = 3
CW3 = 4
ESKW = 5
GES = 6
#                   KD      FP      RO      CW7     CW3     ESKW    GES
hydrophob = {'A': ( 1.8,   0.31,   0.39,   0.35,   0.42,   0.62,   1.6),
             'C': ( 2.5,   1.54,   0.25,   0.76,   0.84,   0.29,   2.0),
             'D': (-3.5,  -0.77,  -3.81,  -2.15,  -0.51,  -0.90,  -9.2),
             'E': (-3.5,  -0.64,  -2.91,  -1.95,  -0.37,  -0.74,  -8.2),
             'F': ( 2.8,   1.79,   2.27,   1.69,   1.74,   1.19,   3.7),
             'G': (-0.4,   0.00,   0.00,   0.00,   0.00,   0.48,   1.0),
             'H': (-3.2,   0.13,  -0.64,  -0.65,  -2.28,  -0.40,  -3.0),
             'I': ( 4.5,   1.8,    1.82,   1.83,   1.81,   1.38,   3.1),
             'K': (-3.9,  -0.99,  -2.77,  -1.54,  -2.30,  -1.50,  -8.8),
             'L': ( 3.8,   1.70,   1.82,   1.80,   1.80,   1.06,   2.8),
             'M': ( 1.9,   1.23,   0.96,   1.10,   1.18,   0.64,   3.4),
             'N': (-3.5,  -0.60,  -1.91,  -0.99,  -1.03,  -0.78,  -4.8),
             'P': (-1.6,   0.72,   0.99,   0.84,   0.86,   0.12,  -0.2),
             'Q': (-3.5,  -0.22,  -1.30,  -0.93,  -0.96,  -0.85,  -4.1),
             'R': (-4.5,  -1.01,  -3.95,  -1.50,  -1.56,  -2.53, -12.3),
             'S': (-0.8,  -0.04,  -1.24,  -0.63,  -0.64,  -0.18,   0.6),
             'T': (-0.7,   0.26,  -1.00,  -0.27,  -0.26,  -0.05,   1.2),
             'V': ( 4.2,   1.22,   1.30,   1.32,   1.34,   1.08,   2.6),
             'W': (-0.9,   2.25,   2.13,   1.35,   1.46,   0.81,   1.9),
             'Y': (-1.3,   0.96,   1.47,   0.39,   0.51,   0.26,  -0.7),
             # the following ones are invented by me (taking into account the
             # values for the more hydrophilic amino acids in each scale.
             's': (-4.5,  -1.01,  -3.95,  -2.15,  -2.28,  -2.53, -12.3),
             't': (-4.5,  -1.01,  -3.95,  -2.15,  -2.28,  -2.53, -12.3),
             'y': (-4.5,  -1.01,  -3.95,  -2.15,  -2.28,  -2.53, -12.3)
             }
#
# Hydropathy from Kyte&Doolittle
# Hydropathy plots allow visualization of hydrophobicity over the length of a
# peptide sequence. A hydropathy scale which is based on the hydrophobic
# and hydrophilic properties of the 20 amino acids is used. A moving "window"
# determines the summed hydropathy at each point in the sequence (Y coordinate).
# These sums are then plotted against their respective positions (X coordinate).
# Such plots are useful in determining the hydrophobic interior portions of
# globular proteins as well as determining membrane spanning regions of membrane
# bound proteins.
# In this calculation the window size is variable, allowing the user to
# change the sensitivity of the calculation. Smaller windows result in "noisier"
# plots than do larger windows. Window sizes between 7-11 residues were found by
# Kyte&Doolittle to maximize the information content of the plots.
# It is advised that the peptide length should be greater than double the window
# size to get any useful information from the Hydropathy plot.
#
hydropathy = {'A': 6.3, 'C': 7.0, 'D': 1.0, 'E': 1.0,
              'F': 7.2, 'G': 4.1, 'H': 1.3, 'I': 9.0,
              'K': 0.6, 'L': 8.2, 'M': 6.4, 'N': 1.0,
              'P': 2.9, 'Q': 1.0, 'R': 0.0, 'S': 3.6,
              'T': 3.8, 'V': 8.7, 'W': 3.6, 'Y': 3.2,
              # B=aspartic or asparagine (D/N),
              # Z=glutamic or glutamine (E/Q) (mod from original)
              'B': 1.0, 'Z': 1.0,
              # J == mean of L-I (invented)
              'J': 8.6,
              # X= unknown amino acid (mean value)
              'X': 3.5,
              # the following ones are invented by me (taking into account
              # the values of the more hydrophilic amino acid R)
              't': 0.0, 's': 0.0, 'y': 0.0
              }

## Isoelectric point (Zimmerman et al., 1968)
# J. Theor. Biol. (1968) v.21, p.170-201
# 6.0000 A ALA
# 10.7600 R ARG
# 5.4100 N ASN
# 2.7700 D ASP
# 5.0500 C CYS
# 5.6500 Q GLN
# 3.2200 E GLU
# 5.9700 G GLY
# 7.5900 H HIS
# 6.0200 I ILE
# 5.9800 L LEU
# 9.7400 K LYS
# 5.7400 M MET
# 5.4800 F PHE
# 6.3000 P PRO
# 5.6800 S SER
# 5.6600 T THR
# 5.8900 W TRP
# 5.6600 Y TYR
# 5.9600 V VAL


## PK VALUES FROM DIFFERENT SOURCES
# Some references:
#  http://www.mhhe.com/physsci/chemistry/carey5e/Ch27/ch27-1-4-2.html
#  http://homepage.smc.edu/kline_peggy/organic/amino_acid_pka.pdf
#  http://upload.wikimedia.org/wikipedia/commons/thumb/5/51/ ->
#        Molecular_structures_of_the_21_proteinogenic_amino_acids.svg/ ->
#        2000px-Molecular_structures_of_the_21_proteinogenic_amino_acids.svg.png
#
# From web docs     a=acid   b=base
pK = {'a': (3.5, 1), 'D': ( 3.65, 1),
                     'E': ( 4.25, 1),
                     'Y': (10.1 , 1),
                     'C': (10.3 , 1),
      'b': (8.0, 0), 'R': (12.5 , 0),
                     'K': (10.5 , 0),
                     'H': ( 6.0 , 0)
    }
# From MassXpert Program, Q didn't have assignation. Values in
#  http://web.indstate.edu/thcme/mwking/amino-acids.html
# are very similar. Q was taken from there
#              ____PK_____         ____PK_____
#              amino  carb         amino  carb
pKab = {'A': ( 9.87, 2.35), 'C': (10.78, 1.92),
        'D': ( 9.90, 1.99), 'E': ( 9.47, 2.10),
        'F': ( 9.18, 2.16), 'G': ( 9.78, 2.35),
        'H': ( 9.33, 1.80), 'I': ( 9.76, 2.32),
                            'X': ( 9.75, 2.32),   #X == mean of L-I
        'K': ( 9.18, 2.16), 'L': ( 9.74, 2.33),
        'M': ( 9.28, 2.13), 'N': ( 8.84, 2.10),
        'P': (10.65, 2.95), 'Q': ( 9.10, 2.20),
        'R': ( 8.99, 1.82), 'S': ( 9.21, 2.19),
        'T': ( 9.10, 2.09), 'V': ( 9.74, 2.29),
        'W': ( 9.44, 2.43), 'Y': ( 9.11, 2.20),
        's': ( 9.21, 2.19), 't': ( 9.10, 2.09),
        'y': ( 9.11, 2.20)
        }
#
# Note Marina pKr phospho amino acids (november 2009)
# These are the phosphopeptide pKs, as they are calculated in
# ScanSite pI/Mw <http://scansite.mit.edu/calc_mw_pi.html>
#
# The isoelectric points are calculated using the algorithm from ExPASy's
# Compute pI/Mw program, which was kindly provided by Elisabeth Gasteiger
# (see also Bjellqvist, B., Hughes, G.J., Pasquali, Ch., Paquet, N., Ravier, F.,
# Sanchez, J.-Ch., Frutiger, S. & Hochstrasser, D.F., /The focusing positions
# of polypeptides in immobilized pH gradients can be predicted from their
# amino acid sequences/, Electrophoresis 1993, 14:1023-1031).
# We have added the option to include phosphorylations,
# using *pKa = 2.12* for the first ionization and *pKa = 7.21* for the second.
#
#             ___PK___        ___PK___
#             R   type        R   type
pKr = {'C': ( 8.30, 1), 'D': ( 3.90, 1),
       'E': ( 4.07, 1), 'H': ( 6.04, 0),
       'K': (10.79, 0), 'R': (12.48, 0),
       'Y': (10.13, 1),
       'a': ( 9.60, 1), 'b': ( 2.10, 0),  # average from pKab
       # ---   phosphorylated ----
       #       R1 type  R2
       's': ( 2.12, 2, 7.21),
       't': ( 2.12, 2, 7.21),
       'y': ( 2.12, 2, 7.21)
       }

## RETENTION COEFFICIENTS IN HPLC
# Meek J.L.                                (MJL2)
#  Proc. Natl. Acad. Sci. USA 77:1632-1636(1980).
#  Retention coefficient in HPLC, pH 2.1
# Meek J.L.                                (MLJ7)
#  Proc. Natl. Acad. Sci. USA 77:1632-1636(1980).
#  Retention coefficient in HPLC, pH 7.4
# Browne C.A., Bennett H.P.J., Solomon S.  (BBS)
#  Anal. Biochem. 124:201-208(1982).
#  Retention coefficient in TFA.
#
MJL2 = 0
MJL7 = 1
BBS = 2
#                 MJL2  MJL7 BBS         MJL2   MJL7 BBS
retent = {'A': ( -0.1,  0.5,  7.3), 'C': (  2.2, -6.8, -9.2),
          'D': ( -2.8, -8.2, -2.9), 'E': ( -7.5,-16.9, -7.1),
          'F': ( 13.9, 13.2, 19.2), 'G': ( -0.5,  0.0, -1.2),
          'H': (  0.8, -3.5, -2.1), 'I': ( 11.8, 13.9,  6.6),
                                    'X': ( 10.9, 11.4, 13.3), # X == mean of L-I
          'K': ( -3.2,  0.1, -3.7), 'L': ( 10.0,  8.8, 20.0),
          'M': (  7.1,  4.8,  5.6), 'N': ( -1.6,  0.8, -5.7),
          'P': (  8.0,  6.1,  5.1), 'Q': ( -2.5, -4.8, -0.3),
          'R': ( -4.5,  0.8, -3.6), 'S': ( -3.7,  1.2, -4.1),
          'T': (  1.5,  2.7,  0.8), 'V': (  3.3,  2.7,  3.5),
          'W': ( 18.1, 14.9, 16.3), 'Y': (  8.2,  6.1,  5.9),
          # the following ones are invented taking into account:
          # O.V.Krokhin,R.Craig,V.Spicer,W.Ens,K.G.Standing,R.C.Beavis,J.A.Wilkins
          # Molecular & Cellular Proteomics, 3, (2004) 908-919.
          # An Improved Model for Prediction of Retention Times of Tryptic
          # Peptides in Ion Pair Reversed-phase HPLC.
          # "The small hydrophilic PO3H2 group has been shown to have only a
          # minor effect on the chromatographic retention of peptides(37),
          # consistent with our own observations"
          # Arbitrarily I subtracted one unit to the corresponding coefficients
          's': ( -4.7, -2.2, -5.1), 't': (  -2.5, 1.7, -0.2),
          'y': (  7.2,  5.1,  4.9)
         }
