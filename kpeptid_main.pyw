#!/usr/bin/env python
# -*- coding: latin-1 -*-
#
"""
_version 3.0.7
3 december 2017
Program for Peptide Analysis
"""
#
#
from matplotlib import use
use('WXAgg')
# noinspection PyPep8
import wx
import string
from commons.warns import tell
from matplotlib import rcParams as rc
from kpeptid_label_panel import GaugeColorPanel
from kpeptid_class import Peptide
from kpeptid_data import ICON
from kpeptid_panels import CanvasPanel, ParamSelectPanel, GaugesPanel
from kpeptid_panels import PeptideSelectPanel, get_scale_data
#
#
TITLE = ' KimPeptid vs 3.0.7'
#
rc['figure.subplot.left'] = 0.14
rc['figure.subplot.right'] = 0.94
rc['figure.subplot.top'] = 0.94
rc['figure.subplot.bottom'] = 0.14
rc['xtick.labelsize'] = 'xx-small'
rc['ytick.labelsize'] = 'xx-small'
rc['axes.labelsize'] = 'small'
rc['font.size'] = 10.0
#
#
# noinspection PyArgumentList,PyUnusedLocal
class PeptideFrame(wx.Frame):
    def __init__(self, title):
        wx.Frame.__init__(self, None, -1, title=title, size=(600, 350))
        self.scales = get_scale_data()
        self.peptide = None
        self.properties = {}
        self.sequence = ''
        self.xvals = []
        self.yvals = []
        sizer = wx.BoxSizer(wx.VERTICAL)      # left
        sizer2 = wx.BoxSizer(wx.HORIZONTAL)   # main sizer
        sizer3 = wx.BoxSizer(wx.VERTICAL)     # center
        sizer4 = wx.BoxSizer(wx.HORIZONTAL)   # auxiliary center above
        sizer5 = wx.BoxSizer(wx.VERTICAL)     # right
        sizer6 = wx.BoxSizer(wx.HORIZONTAL)   # auxiliary center below
        #
        self.Bind(wx.EVT_CLOSE, self.on_close_window)
        self.Bind(wx.EVT_SIZE, self.on_size)
        #
        self.canvas_charge = CanvasPanel(self,
                                         xlabel='pH', ylabel='charge',
                                         tx='pH', ty='Q')
        self.canvas_hdpt = CanvasPanel(self,
                                       xlabel='AA pos', ylabel='hydropathy',
                                       tx='pos', ty='hdpt')
        self.canvas_charge.canvas.mpl_connect('motion_notify_event',
                                              self.on_motion1)
        self.canvas_hdpt.canvas.mpl_connect('motion_notify_event',
                                            self.on_motion2)
        self.tc_sequence = wx.TextCtrl(self,
                                       size=(50, 100),
                                       style=wx.TE_MULTILINE | wx.TE_RICH2)
        # get background color to change and recover when bad sequence
        self.default_tc_color = self.tc_sequence.GetBackgroundColour()

        self.params = ParamSelectPanel(self)
        #
        self.gauges = GaugesPanel(self, size=(250, 110))
        #
        self.lb_mass = wx.StaticText(self, -1, label='mass monoisotopic',
                                     size=(130, 20))
        self.tc_mass = wx.TextCtrl(self, size=(65, 20))
        #
        self.bt_go = wx.Button(self, label='GO', size=(20, 25))
        self.Bind(wx.EVT_BUTTON, self.on_go, self.bt_go)
        self.bt_add = wx.Button(self, label='ADD', size=(20, 25))
        self.Bind(wx.EVT_BUTTON, self.on_add_to_list, self.bt_add)
        self.bt_clear = wx.Button(self, label='CLEAN', size=(20, 25))
        self.Bind(wx.EVT_BUTTON, self.on_clear, self.bt_clear)
        #
        ss = self.select = PeptideSelectPanel(self)
        self.Bind(wx.EVT_LISTBOX_DCLICK, self.on_click_list, ss.chklst)
        self.Bind(wx.EVT_LISTBOX, self.on_select_peptide, ss.chklst)
        self.Bind(wx.EVT_BUTTON, self.on_show, ss.bt_show)
        self.Bind(wx.EVT_BUTTON, self.on_refresh, ss.properties['Refresh'])
        self.canvas_2d = CanvasPanel(self,
                                     xlabel='LC SCX', ylabel='LC RP',
                                     tx='SCX', ty='RP')
        self.canvas_2d.canvas.mpl_connect('button_press_event',
                                          self.on_click_canvas_2d)
        # left
        sizer.Add(self.canvas_charge, 1, wx.EXPAND)
        sizer.Add(self.canvas_hdpt, 1, wx.EXPAND)
        # aux center above
        sizer4.Add(self.lb_mass, 0, wx.EXPAND)
        sizer4.Add(self.tc_mass, 0, wx.EXPAND)
        sizer4.Add(wx.Panel(self, size=(10, 10)), 1, wx.EXPAND)
        # aux center
        sizer6.Add(self.bt_add, 1, wx.EXPAND)
        sizer6.Add(self.bt_go, 1, wx.EXPAND)
        sizer6.Add(self.bt_clear, 1, wx.EXPAND)
        # center
        sizer3.Add(self.tc_sequence, 1, wx.EXPAND)
        sizer3.Add(sizer4, 0, wx.EXPAND)
        sizer3.Add(sizer6, 0, wx.EXPAND)
        sizer3.Add(wx.Panel(self, size=(-1,15)), 0, wx.EXPAND)
        sizer3.Add(self.params, 0, wx.EXPAND)
        sizer3.Add(wx.Panel(self, size=(-1,15)), 0, wx.EXPAND)
        sizer3.Add(self.gauges, 1, wx.EXPAND)
        sizer3.Add(GaugeColorPanel(self), 0, wx.EXPAND)
        # right
        sizer5.Add(wx.Panel(self, size=(10, 10)), 0, wx.EXPAND)
        sizer5.Add(self.select, 1, wx.EXPAND)
        sizer5.Add(wx.Panel(self, size=(10, 10)), 0, wx.EXPAND)
        sizer5.Add(self.canvas_2d, 1, wx.EXPAND)
        # main
        sizer2.Add(sizer, 3, wx.EXPAND)
        sizer2.Add(sizer3, 2, wx.EXPAND)
        sizer2.Add(sizer5, 3, wx.EXPAND)
        #
        self.SetSizer(sizer2)
        self.Fit()
        #
        _icon = wx.Icon(ICON, wx.BITMAP_TYPE_ICO)
        self.SetIcon(_icon)
        #
        self.SetMinSize((700, 500))          # frame min size
        self.SetSize((900, 650))
        #
        for key in self.select.peptides:
            self.select.chklst.Append(key)
    #
    def on_size(self, evt):
        self.Refresh()
        evt.Skip()
    #
    def on_refresh(self, evt):
        kwargs = self.get_pept_local_defaults()
        self.set_pept_global_defaults(kwargs)
        for key, pept in self.select.peptides.items():
            self.select.peptides[key] =\
                Peptide(pept.sequence,
                        hydrophobicity=kwargs['hydrophobicity'],
                        retention=kwargs['retention'],
                        window=kwargs['window'])
    #
    def on_clear(self, evt):
        self.tc_sequence.Clear()
    #
    def on_show(self, evt):
        """"""
        self.xvals = []
        self.yvals = []
        for peptide in self.select.peptides.values():
            self.xvals.append(peptide.Q3)
            self.yvals.append(peptide.Rt)
        self.canvas_2d.axes.clear()
        self.draw_canvas_2d(True)
    #
    def draw_canvas_2d(self, refresh=True):
        self.canvas_2d.axes.plot(self.xvals, self.yvals, 'r+')
        self.canvas_2d.draw(refresh)
    #
    # noinspection PyArgumentEqualDefault
    def on_select_peptide(self, evt):
        two_d = self.canvas_2d
        two_d.xlim = two_d.axes.viewLim.intervalx
        two_d.ylim = two_d.axes.viewLim.intervaly
        peptide_name = self.select.chklst.GetStringSelection()
        self.peptide = self.select.peptides[peptide_name]
        two_d.axes.clear()
        self.draw_canvas_2d(refresh=False)
        two_d.axes.plot([self.peptide.Q3], [self.peptide.Rt], 'go')
        if self.xvals:
            two_d.draw(True)
        else:
            two_d.draw(False)
        #
        if self.peptide.is_bad_sequence():
            self.tc_sequence.SetBackgroundColour('red')
        else:
            self.tc_sequence.SetBackgroundColour(self.default_tc_color)

        self.tc_sequence.SetValue(self.peptide.sequence)
        self.draw_all_panels()
    #
    def draw_all_panels(self):
        self.draw_mass()
        self.draw_pi()
        self.draw_hydropathy()
        self.draw_gauges()
    #
    def on_motion1(self, evt):
        if evt.inaxes:
            xpos, ypos = evt.xdata, evt.ydata
            try:
                y = self.peptide.calc_q(xpos)
            except AttributeError:
                return
            self.canvas_charge.tc_x.SetValue('%.1f' % xpos)
            self.canvas_charge.tc_y.SetValue('%.1f' % y)
    #
    def on_motion2(self, evt):
        if evt.inaxes:
            xpos, ypos = evt.xdata, evt.ydata
            xpos = int(round(xpos))
            self.canvas_hdpt.tc_x.SetValue('%i' % xpos)
            self.canvas_hdpt.tc_y.SetValue('%.1f' % ypos)
            self.mark_amino_acid(xpos)
    #
    def on_click_canvas_2d(self, evt):
        if evt.inaxes:
            xpos, ypos = evt.xdata, evt.ydata
            self.canvas_2d.tc_x.SetValue('%.1f' % xpos)
            self.canvas_2d.tc_y.SetValue('%.1f' % ypos)
            try:
                self.mark_peptide_in_list(xpos, ypos)
            except ValueError:
                pass
    #
    # noinspection PySimplifyBooleanCheck
    def mark_peptide_in_list(self, xpos, ypos):
        """Marks as selected the peptide closer to <xpos>, <ypos> in canvas"""
        alist = []
        range1 = max(self.xvals) - min(self.xvals)
        range2 = max(self.yvals) - min(self.yvals)
        if range1 == 0:
            range1 = 0.0001      # to prevent Divide by zero
        if range2 == 0:
            range2 = 0.0001
        # calculates distance of peptide to xpos,ypos
        for i in range(len(self.xvals)):
            a = ((self.xvals[i] - xpos) / range1) ** 2
            b = ((self.yvals[i] - ypos) / range2) ** 2
            alist.append((a + b, i))
        alist.sort()
        index = alist[0][1]
        try:
            keys = list(self.select.peptides.keys())
            peptide_key = keys[index]
        except IndexError:
            return
        self.select.chklst.SetStringSelection(peptide_key, 1)
    #
    def mark_amino_acid(self, aa_pos):
        try:
            sec = self.peptide.sequence[:]
        except AttributeError:
            return
        #
        character = 0             # 1 --> #AA + #blancos
        aa = 0                    # 1 --> #AA
        while aa < aa_pos:
            try:
                letter = sec[character]
                if letter in string.ascii_letters:
                    aa += 1
            except IndexError:
                return
            character += 1
        #
        style = self.tc_sequence.GetBackgroundColour()
        self.tc_sequence.Clear()
        self.tc_sequence.SetValue(sec)
        self.tc_sequence.SetStyle(character - 1, character,
                                  wx.TextAttr("white", "red"))
    #
    def on_add_to_list(self, evt):
        """"""
        sec = self.tc_sequence.GetValue()

        if len(sec) < 1:
            return
        #
        try:
            kwargs = self.get_pept_global_defaults()
        except ValueError:
            kwargs = self.get_pept_local_defaults()
            self.set_pept_global_defaults(kwargs)
        #
        self.peptide = Peptide(sec,
                               hydrophobicity=kwargs['hydrophobicity'],
                               retention=kwargs['retention'],
                               window=kwargs['window'])
        #
        if self.peptide.is_bad_sequence():
            forget = tell('This sequence has some non accepted Amino Acid', 'Wrong Amino Acid', 'question')
            if not forget:
                return
        #
        dialog = wx.TextEntryDialog(None, "Set Peptide Name",
                                    "ADD TO LIST", sec[0:9],
                                    style=wx.OK | wx.CANCEL)
        key = ''
        if dialog.ShowModal() == wx.ID_OK:
            key = dialog.GetValue()
        else:
            dialog.Destroy()
            return
        #
        key_old = key
        i = 0
        while key in self.select.peptides:
            i += 1
            key = '%s_%i' % (key_old, i)
        #
        try:
            self.select.chklst.Append(key)
        except UnboundLocalError:
            return
        #
        self.select.peptides[key] = self.peptide
        self.draw_all_panels()
    #
    def get_pept_global_defaults(self):
        kwargs = {}
        for name in self.scales:
            scale = self.select.peptide_defaults.properties[name].GetValue()
            kwargs[name] = self.scales[name].index(scale, 3) - 3
        return kwargs
    #
    def get_pept_local_defaults(self):
        kwargs = {}
        for name in self.scales:
            scale = self.params.properties[name].GetValue()
            kwargs[name] = self.scales[name].index(scale, 3) - 3
        return kwargs
    #
    def set_pept_global_defaults(self, kwargs):
        for key, value in kwargs.items():
            name = self.scales[key][value + 3]
            self.select.peptide_defaults.properties[key].SetValue(name)
    #
    def on_click_list(self, evt):              # No need for double click
        peptide_name = self.select.chklst.GetStringSelection()
        self.peptide = self.select.peptides[peptide_name]
        self.tc_sequence.SetValue(self.peptide.sequence)
        self.draw_all_panels()
    #
    def on_go(self, evt):
        sequence = self.tc_sequence.GetValue()
        if len(sequence) < 1:
            return
        #
        kwargs = self.get_pept_local_defaults()
        #
        self.peptide = Peptide(sequence,
                               hydrophobicity=kwargs['hydrophobicity'],
                               retention=kwargs['retention'],
                               window=kwargs['window'])
        self.draw_all_panels()
    #
    def draw_mass(self):
        self.tc_mass.SetValue('%.2f' % self.peptide.Mr)
    #
    def draw_pi(self):
        ph_range, charge = self.peptide.plot_charge()
        self.canvas_charge.axes.clear()
        self.canvas_charge.axes.plot(ph_range, charge)
        self.canvas_charge.draw()

    def draw_hydropathy(self):
        """"""
        hydropathy_plot = self.peptide.plot_hydropathy()
        # print(hydropathy_plot)
        new_list = []
        aa_number = 0
        width = int(self.params.properties['window'].GetValue())
        for item in self.peptide.sequence:
            if item.isalpha():
                aa_number += 1
                value = 0
                n = 0
                for j in range(round(-width / 2), round(width / 2)):
                    try:
                        value += hydropathy_plot[aa_number + j]
                        n += 1
                    except IndexError:
                        pass
                #
                try:
                    new_list.append(value / n)
                # unknown amino acids can produce n = 0
                except ZeroDivisionError:
                    tell('hydropathy_plot\nZeroDivision error')
        #
        locus = range(1, len(new_list) + 1)
        #
        self.canvas_hdpt.axes.clear()
        self.canvas_hdpt.axes.plot(locus, new_list)
        self.canvas_hdpt.draw()
    #
    def draw_gauges(self):
        """"""
        for key, gobject in self.gauges.gobjects.items():
            valor = getattr(self.peptide, key)
            gobject.update_gauges(valor)
            gobject.Refresh()

    def on_close_window(self, evt):
        self.Destroy()
#
#
#
if __name__ == '__main__':

    app = wx.App()
    PeptideFrame(title=TITLE).Show()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
