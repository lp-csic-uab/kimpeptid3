#!/usr/bin/env python
# -*- coding: latin-1 -*-
"""
version 0.68
13 october 2010
Program for Peptide Analysis
"""
#
#
from numpy import linspace
from kpeptid_data import KD, MJL2, pKab, pKr
from kpeptid_data import hydropathy, retent, hydrophob
from commons.mass import get_mass, AA
#
#
TITLE = ' KimPeptid vs.0.68'
#
#
# noinspection PyPep8Naming
class Peptide:
    """A peptide class with sequence and properties"""
    def __init__(self, sequence, hydrophobicity=KD, retention=MJL2, window=0):
        """Constructor

        -sequence -> accepts s, t y
        -retention
        -window -> width of sequence for the calculation of the hydropathy plot

        """
        self.sequence = sequence
        self.hydrophobicity = hydrophobicity
        self.retention = retention
        self.window = window

        self.Q3 = self.calc_q(3)
        self.Rt = self.calc_retention()
        self.pI = self.calc_pi(0.001)
        self.Mr = self.calc_mr()
        #
        (self.AH, self.percent_phobics,
         self.percent_philics, self.RH, self.RHP,
         self.LIND, self.sol) = self.calc_hydrophob_params(hydrophobicity)

    def is_bad_sequence(self):
        """flag to label the peptide as having some unknown AA symbol"""
        for aa in self.sequence:
            if aa not in AA:
                return True
    #
    # noinspection PyStringFormat
    def calc_q(self, pH=7):
        """Calculates peptide charges at a given pH."""
        amino = self.sequence[0]
        carboxyl = self.sequence[-1]
        #
        try:
            pKr['b'] = (pKab[amino][0], 0)
        except KeyError:
            pKr['b'] = (9.6, 0)                              # mean value
            print('no amino = %s...' % self.sequence[:5])
        #
        try:
            pKr['a'] = (pKab[carboxyl][1], 1)
        except KeyError:
            pKr['a'] = (2.1, 1)                              # mean value
            print('no carboxyl = ...%s' % self.sequence[-5:])
        #
        sequence = self.sequence + 'ab'
        Q = 0
        for amino_acid in sequence:
            try:
                protons = 1 / (1 + 10 ** (pH - pKr[amino_acid][0]))
                if amino_acid in ['s', 't', 'y']:
                    protons += 1 / (1 + 10 ** (pH - pKr[amino_acid][2]))
                charge = protons - pKr[amino_acid][1]
                Q += charge
            except KeyError:
                pass
        return Q
    #
    def plot_charge(self):
        """"""
        charges = []
        my_range = linspace(1, 14, 40)     # incr 0.333
        for pH in my_range:
            charges.append(self.calc_q(pH))

        return my_range, charges

    def calc_pi(self, error=0.01):
        """"""
        start = 2
        stop = 14
        while stop - start > error:
            new_stop = start + (stop - start) / 2.
            if self.calc_q(new_stop) < 0:
                stop = new_stop
            else:
                start = new_stop
        return start

    def calc_mr(self):
        """Calculates relative mass of peptide sequence"""
        return get_mass(self.sequence)

    def plot_hydropathy(self):
        """"""
        hydropathy_plot = []
        for amino_acid in self.sequence:
            try:
                hydropathy_plot.append(hydropathy[amino_acid])
            except KeyError:
                hydropathy_plot.append(hydropathy['X'])

        return hydropathy_plot

    def calc_retention(self):
        """"""
        ret = 0
        for amino_acid in self.sequence:
            try:
                ret += retent[amino_acid][self.retention]
            except KeyError:
                pass
        return ret

    def calc_hydrophob_params(self, hydrophobicity=0):
        """Calculates hydrophobicity variables, charge density and solubility

        Data from David Wishart, "Protein Expression, Structural Proteomics &
        Bioinformatics", Lecture 3.0 Bioinformatics.ca
        (http://bioinformatics.ca/workshop_pages/proteomics2005/index.html)

        Average Hydrophobicity          Average AH = 2.5 + 2.5
            AH = S AAi x Hi             Insol > 0.1    Unstrc < -6
        Hydrophobic Ratio               Average RH = 1.2 + 0.4
            RH = S H(-)/S H(+)          Insol < 0.8  Unstrc > 1.9
        Hydrophobic % Ratio             Average RHP = 0.9 + 0.2
            RHP = %philics/%phobic       Insol < 0.7  Unstrc > 1.4
        Linear Charge Density           Average LIND = 0.25
            LIND = (K+R+D+E+H+2)/#      Insol < 0.2  Unstrc > 0.4
        Solubility                      Average SOL = 1.6 + 0.5
            SOL = RH + LIND - 0.05AH    Insol < 1.1  Unstrc > 2.5

        Returns (AH, ret, percent_phobics, percent_philics, RH, RHP, LIND, sol)

        """
        #
        phobics = 0
        philics = 0
        hpos = 0.
        hneg = 0.
        ionics = 0
        #
        length = float(len(self.sequence))
        for amino_acid in self.sequence:
            #
            if amino_acid in 'ACFGILMVWY':
                phobics += 1
            elif amino_acid in 'KRDEH':
                ionics += 1
                philics += 1
            elif amino_acid in 'NPQST':
                philics += 1
            #
            try:
                valor = hydrophob[amino_acid][hydrophobicity]
                if valor > 0:
                    hpos += valor
                else:
                    hneg += valor
            except KeyError:
                pass

        AH = hpos + hneg
        percent_phobics = phobics * 100. / length
        percent_philics = philics * 100. / length
        try:
            RH = -hneg / hpos
        except ZeroDivisionError:
            RH = 1000
        #
        # this is the same as percent_philics / percent_phobics
        try:
            RHP = philics / phobics
        except ZeroDivisionError:
            RHP = 1000
        #
        LIND = (ionics + 2) / length
        sol = RH + LIND - (0.05 * AH)
        #
        return AH, percent_phobics, percent_philics, RH, RHP, LIND, sol

if __name__ == '__main__':
    # to work in interactive requires TkAgg, no WXAgg
    from matplotlib import use
    use('TkAgg')
    from pylab import plot, figtext, xlabel, ylabel, show
    #
    default_sec = 'PRLFEGHEISWQK'   # PRK, EE
    while True:
        sec = input('Enter sequence= ')
        if sec == 'q':
            break
        if not sec:
            sec = default_sec
        pep = Peptide(sec)
        ph_range, charge = pep.plot_charge()
        plot(ph_range, charge)
        xlabel('pH')
        ylabel('charge total')
        text = []
        for item in ['pI', 'AH', 'RH', 'RHP', 'LIND', 'sol']:
            # noinspection PyStringFormat
            val = '%-7s ==> %6.2f' % (item, getattr(pep, item))
            text.append(val)

        i = 0.85
        for item in text:
            figtext(0.4, i, item)
            i -= 0.05
        show()
